package com.sasipa.week11;

public interface Flyable {
    public void fly();
    public void landing();
    public void takeoff();
}
