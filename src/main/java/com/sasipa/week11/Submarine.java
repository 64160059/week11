package com.sasipa.week11;

public class Submarine extends Vahcle implements Swimable{

    public Submarine(String name, String engieName) {
        super(name, engieName);
    }

    @Override
    public void swim() {
        System.out.println(this + " swim ");
        
    }
    @Override
    public String toString() {
        return "Submarine("+this.getName() +") " + " engine: " + this.getEngieName();
    }
    
}
