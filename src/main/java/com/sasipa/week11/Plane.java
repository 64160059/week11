package com.sasipa.week11;

public class Plane extends Vahcle implements Flyable{

    public Plane(String name, String engieName) {
        super(name, engieName);
        
    }

    @Override
    public void fly() {
        System.out.println(this.toString() + " fly. ");
        
    }

    @Override
    public void landing() {
        System.out.println(this.toString() + " landing. ");
        
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString() + " takeoff. ");
        
    }
    @Override
    public String toString() {
        return "Plane("+this.getName() +") " + " engine: " + this.getEngieName();
    }
    
}
