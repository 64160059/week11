package com.sasipa.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();

        Plane plane1 = new Plane("Boing", "Boing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Crocodile crocodile1 = new Crocodile("papzie");
        crocodile1.crawl();
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();

        Bird bird1 = new Bird("Blue");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        Snake snake1 = new Snake("green");
        snake1.crawl();
        snake1.eat();
        snake1.sleep();

        Submarine submarine1 = new Submarine("Marine Ford", "Marine engie");
        submarine1.swim();

        Cat cat1 = new Cat("Sevanup");
        cat1.eat();
        cat1.run();
        cat1.sleep();
        cat1.walk();

        Dog dog1 = new Dog("PangPong");
        dog1.eat();
        dog1.run();
        dog1.sleep();
        dog1.walk();

        Human human1 = new Human("Earn");
        human1.eat();
        human1.run();
        human1.sleep();
        human1.walk();

        Rat rat1 = new Rat("NatCha");
        rat1.eat();
        rat1.run();
        rat1.sleep();
        rat1.walk();

        Flyable[] flyablesobjects = { bat1, plane1, bird1 };
        for (int i = 0; i < flyablesobjects.length; i++) {
            flyablesobjects[i].takeoff();
            flyablesobjects[i].fly();
            flyablesobjects[i].landing();
        }
        Crawlable[] crawlableobjects = { snake1, crocodile1 };
        for (int i = 0; i < crawlableobjects.length; i++) {
            crawlableobjects[i].crawl();

        }
        Swimable[] swimableobjects = { fish1, submarine1, crocodile1 };
        for (int i = 0; i < swimableobjects.length; i++) {
            swimableobjects[i].swim();
        }
        Walkable[] walkablesobjects = {cat1, dog1, human1};
        for (int i = 0; i < walkablesobjects.length; i++) {
            walkablesobjects[i].walk();
            
        }
    }

}
